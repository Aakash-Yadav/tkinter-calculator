# Tkinter Calculator

Tkinter calculator is a simple Graphical User Interface calculator developed by using the Tkinter module.


### Definition of Tkinter Calculator

Tkinter calculator is a simple Graphical User Interface calculator developed by using the Tkinter module. Python offers many ways for developing various GUI applications. Tkinter module is one of the most useful and easiest ways for developing GUI applications because Tkinter works well in both Linux and Windows platforms. In this article, we will see how to develop a simple GUI calculator using the Tkinter module.

##### Prerequisites for developing Tkinter Calculator:

1. pip3 install tk 


### How to Create a simple GUI Tkinter Calculator?

We can perform the following basic arithmetic operations.

1. Addition
2. Subtraction
3. Division
4. Multiplication
5. Power 
6. square
7. square root 

### Output:
![alt text](https://xp.io/storage/1Po4zP8f.png)
